/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 8, 2016 3:39:33 PM                      ---
 * ----------------------------------------------------------------
 */
package my.bookstore.initialdata.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedBookstoreInitialDataConstants
{
	public static final String EXTENSIONNAME = "bookstoreinitialdata";
	
	protected GeneratedBookstoreInitialDataConstants()
	{
		// private constructor
	}
	
	
}
